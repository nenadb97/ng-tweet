/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserService } from './user.service';
import {AppComponent} from "./app.component";
import {FeedComponent} from "./feed/feed.component";
import {MenuComponent} from "./menu/menu.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

describe('Service: User', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService],
      declarations:[AppComponent, FeedComponent, MenuComponent],
      imports:[CommonModule,FormsModule]
    });
  });

  it('should return static value of Nenad', inject([UserService], (service: UserService) => {
    expect(service.getCurrentUser()).toBe("Nenad");
  }));
});

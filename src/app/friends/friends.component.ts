import { Component, OnInit } from '@angular/core';
import {FeedService} from '../feed.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  friends: Observable<string[]>;
  constructor(private feedService: FeedService) {

  }

  ngOnInit() {
    this.feedService.getFriends().subscribe((friends: string []) => {
      console.log(friends);
    });
    this.friends = this.feedService.getFriends();
    //console.log(this.friends);
  }

}

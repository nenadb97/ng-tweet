import { Injectable } from '@angular/core';
import {UserService} from '../app/user.service';
import {Tweet} from './tweet';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
@Injectable()
export class FeedService {

  tweets = [
    new Tweet('Only passions, great passions, can elevate the soul to great things.', 'Glan', new Date, [], ['Marry']),
    new Tweet('We all have big changes in our lives that are more or less a second chance.', 'Marry', new Date, [], ['Nenad']),
    new Tweet('Personality can open doors, but only character can keep them open.', 'Joe', new Date, [], ['Glan']),
    new Tweet('Always render more and better service than is expected of you, no matter what your task may be.', 'Jack', new Date, [], ['Marry']),

  ];

  constructor(private userService: UserService, private http:Http) { }

  //Get all friends
  getFriends():Observable<string[]>{
    return this.http.get('../assets/friends.json')
      .map((res: Response) => res.json() as string []);
  }

  // Get all tweets from array
  getCurrentFeed():Array<Tweet> {
    return this.tweets;
  }

  // Check if user is in tweet fields likes or retweets
  private isUserInCollection(collection:string[], userId:string,):boolean{
    return collection.indexOf(userId) != -1;
  }

  // Post new tweet
  postNewTweet(tweetText:string){
    this.tweets.unshift(
      new Tweet(tweetText,this.userService.getCurrentUser(),new Date,[], [])
    );
  }

  // Retweet
  retweet(tweet:Tweet){
    if (!this.isUserInCollection(tweet.retweets,this.userService.getCurrentUser())){
      tweet.retweets.push(this.userService.getCurrentUser());
    }
  }

  // Favorite tweet
  favoriteTweet(tweet:Tweet){
    if (!this.isUserInCollection(tweet.favorites,this.userService.getCurrentUser())){
      tweet.favorites.push(this.userService.getCurrentUser());
    }
  }

}
